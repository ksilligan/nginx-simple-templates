#!/bin/sh

set -x

pid=0

term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

trap 'kill ${!}; term_handler' SIGTERM

cat /etc/nginx/conf.d/default.conf | envsubst '${APP_BASE_PREFIX}'> /etc/nginx/conf.d/default.conf
cat /etc/nginx/conf.d/default.conf
echo 'starting'

nginx -g "daemon off;" &

pid="$!"

while true
do
  tail -f /dev/null & wait ${!}
done
